Rails.application.routes.draw do

  constraints host: 'www.disruptiondiagnostic.com' do
    match '(*any)', to: redirect(host: 'disruptiondiagnostic.com'), via: :all
  end

  constraints host: 'scurvelocator.com' do
    match '(*any)', to: redirect(host: 'disruptiondiagnostic.com'), via: :all
  end

  constraints host: 'www.scurvelocator.com' do
    match '(*any)', to: redirect(host: 'disruptiondiagnostic.com'), via: :all
  end

  root to: "visitor#home"

  get  'start_survey', to: redirect('/')
  post 'start_survey', to: 'visitor#start_survey'
  get  'question/:id', to: 'visitor#question',    as: 'q'
  get  'about-you-1',  to: 'visitor#about_you_1', as: 'about_you_1'
  get  'about-you-2',  to: 'visitor#about_you_2', as: 'about_you_2'
  patch 'about-yous',  to: 'visitor#update_about_yous',  as: 'about_yous'
  post 'answer/:id',   to: 'visitor#answer',      as: 'a'
  get  'results',      to: 'visitor#results'

  devise_for :users, path: 'auth', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
  devise_scope :user do
    get 'register', to: 'devise/registrations#new'
    get 'login', to: 'devise/sessions#new'
    get 'logout', to: 'devise/sessions#destroy', as: 'logout'
  end

  scope :admin, module: 'admin' do
    root to: 'surveys#index'
    resources :questions, except: [:new, :create, :destroy]
    resources :surveys do
      get 'results/:code', on: :collection, to: 'surveys#company_results', as: :company_results
    end
    scope :charts do
      root to: 'charts#index', as: 'charts'
      get 'data', to: 'charts#data', as: 'charts_data'
    end
    scope :orgs do
      root to: 'orgs#index', as: 'orgs'
      get "new", to: "orgs#new", as: 'new'
      post "create", to: "orgs#create", as: 'create'
      patch "update_team", to: "orgs#update_team", as: 'update_team'
      post "create_team", to: "orgs#create_team", as: 'create_team'
    end
  end
end
