// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
//= require jquery_ujs
//= require popper
//= require bootstrap-sprockets
//= require activestorage
//= require_tree .

function generateChart(score) {
    var svg, pathData, path, circleGroup, circle;

    pathData = "M1 421C145.5 418.503 285.432 416.813 434.5 207.767C580.5 3.02388 723.5 2.69098 868 3.0239";
    svg = d3.select("#chartContainer").append("svg").attr("class", "sCurve").attr("preserveAspectRatio", "xMinYMin meet").attr("viewBox", "0 0 869 424");
    path = svg.append("path").attr("d", pathData);
    circleGroup = svg.append("g");
    circle = circleGroup.append("circle").attr("r", 25);
    circleGroup.append("text").text(score).attr("id", "scoreText").attr("fill", "#fff").attr("y", "7").attr("x", "-5").attr("font-size", "20");
    circleGroup.transition().duration(3000).attrTween("transform", translateAlong(path.node(), score));
}

function translateAlong(path, score) {
    if (score > 99) {
        score = 99;
    }
    let l = path.getTotalLength() * score / 100;
    return function(d, i, a) {
        return function(t) {
            var p = path.getPointAtLength(t * l);
            return "translate(" + p.x + "," + p.y + ")";
        };
    };
}

$(document).ready(function() {
  var scoreText = document.getElementById("score");
  if (!scoreText) {
    return;
  }
  var score = parseInt(scoreText.textContent.trim(), 10);
  if (isNaN(score)) {
    return;
  }
  generateChart(score);
});

if ("serviceWorker" in navigator) {
  navigator.serviceWorker.getRegistrations().then(function(registrations) {
   for (let registration of registrations) {
      registration.unregister();
    }
  });
}
