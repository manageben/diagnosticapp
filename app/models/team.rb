class Team < ApplicationRecord
  belongs_to :organisation, optional: true

  before_save :assign_organisation

  before_save :downcase_name

  def downcase_name
    self.name = self.name.downcase
  end

  def assign_organisation
    if !self.organisation_id
      self.organisation_id = Organisation.random_organisation.id
    end    
  end

  def self.random_team
    find_by_name("Random team")
  end

end
