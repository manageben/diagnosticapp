class Organisation < ApplicationRecord
  has_many :teams

  validate :check_uniq_name
  before_save :downcase_name

  def downcase_name
    self.name = self.name.downcase
  end

  def self.random_organisation
    find_by_name("random organisation")
  end

  def check_uniq_name
    if Organisation.exists?(name: self.name.downcase)
       errors.add(:name, "must be uniq")
    end
  end

end
