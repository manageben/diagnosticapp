class Survey < ApplicationRecord
  has_many :answers, dependent: :destroy
  belongs_to :team, optional: true
  validates :name, :email, presence: true

  before_save :assign_team

  def assign_team
    if self.team_name
      team = Team.find_or_create_by(name: self.team_name.downcase)
    else
      team = Team.random_team
    end
    self.team_id = team.id
  end

  def score
    answers.map(&:answer_before_type_cast).reduce(&:+)
  end

  def first_name
    if name.split.count > 1
      name.split[0..-2].join(' ')
    else
      name
    end
  end

  def last_name
    if name.split.count > 1
      name.split.last
    end
  end

  def self.average_score_for(team_id)
    return nil if team_id.blank?

    Survey.where(team_id: team_id).map(&:score).compact.then{ |arr| arr.inject(:+).to_f / arr.length }
  end
end
