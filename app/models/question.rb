class Question < ApplicationRecord
  has_many :answers, dependent: :destroy
  validates :category, :low, :mid, :high, presence: true

  def self.start
    1
  end

  def self.end
    self.all.count
  end
end
