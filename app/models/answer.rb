class Answer < ApplicationRecord
  belongs_to :question
  belongs_to :survey

  enum answer: {
    low: 1,
    mid: 5,
    high: 10,
  }

  def answer_text
    if low?
      question.low
    elsif mid?
      question.mid
    elsif high?
      question.high
    else
      "not yet answered"
    end
  end

  def initial
    if low?
      "L"
    elsif mid?
      "M"
    elsif high?
      "H"
    else
      "—"
    end
  end
end
