class Admin::OrgsController < ApplicationController

  def index
    @random_organisation = Organisation.random_organisation
    @random_teams = @random_organisation.teams
    @all_organisations = Organisation.all - [@random_organisation]
  end

  def new
    @organisation = Organisation.new
  end

  def create
    @organisation = Organisation.new(organisation_params)
    respond_to do |format|
      if @organisation.save
        format.html { redirect_to orgs_url }
      else
        format.html { render :new }
      end
    end
  end
  
  def update_team
    @team = Team.find(params[:team_id])
    @organisation = Organisation.find(params[:org_id])
    @random_organisation = Organisation.random_organisation
    @all_organisations = Organisation.all - [@random_organisation]
    if @team.update(organisation_id: @organisation.id)
      render json: {message: "Team is updated successfully!"}
    end
  end

  def create_team
    @organisation = Organisation.where(id: params[:org_id]).first || Organisation.random_organisation

    @team = Team.new(name: params[:team_name], organisation_id: @organisation.id)
    
    if @team.save
      render json: {message: "Team is created successfully!"}
    end
  end

  private

  def organisation_params
    params.require(:organisation).permit(:name)
  end
end
