class Admin::ChartsController < ApplicationController

  def index
    @questions = Question.all
  end

  def data
    @questions = Question.all
    render json: @questions
  end

end
