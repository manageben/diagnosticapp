class VisitorController < ApplicationController
  skip_before_action :authenticate_user!
  before_action :set_survey, only: [:question, :answer, :about_you_1, :about_you_2, :update_about_yous]

  def home
    @survey = Survey.new
  end

  def start_survey
    @survey = Survey.new(survey_params)
    
    respond_to do |format|
      if @survey.save
        session[:survey_id] = @survey.id
        format.html { redirect_to q_url(1) }
        save_user_to_convertkit(@survey)
      else
        format.html { render :home }
      end
    end
  end

  def question
    @question = Question.find_by(sort_order: params[:id])
    @answer = Answer.find_or_initialize_by(survey: @survey, question: @question)
    @page_number = params[:id].to_i
    @progress_pct = 100*@page_number/Question.end
  end

  def about_you_1
  end

  def about_you_2
  end

  def update_about_yous
    @survey.update(survey_params)
  end

  def answer
    @question = Question.find_by(sort_order: params[:id])
    @answer = Answer.find_or_initialize_by(survey: @survey, question: @question)
    @answer.update!(answer_params)
  end

  def results
    if params[:score].present?
      @score = params[:score].to_i
    else
      set_survey
      return if @survey.blank?
      @score = @survey.score
    end
    respond_to do |format|
      format.html
      format.pdf do
        pdf = Tempfile.new("pdf-chrome-puppeteer").path + '.pdf'
        url = results_url(score: @score)
        system("node save-as-pdf.js #{Shellwords.escape(url)} #{Shellwords.escape(pdf)}")
        send_file(pdf, filename: "results.pdf", type: 'application/pdf', disposition: 'inline')
      end
    end
  end

  private

  def set_survey
    unless @survey = Survey.find_by_id(session[:survey_id])
      redirect_to root_url
    end
  end

  def survey_params
    params.require(:survey).permit(:name, :email, :team_name, :age, :industry, :how_long_in_role, :gender, :intention_to_use_feedback, :next_steps, :what_information, :how_can_we_improve)
  end

  def answer_params
    params.require(:answer).permit(:answer)
  end

  def save_user_to_convertkit(survey)
    api_key = ENV.fetch("CONVERTKIT_API_KEY") { nil }

    return if api_key.blank?

    body = JSON.generate({
      api_key: api_key,
      email: survey.email,
      first_name: survey.first_name,
      fields: {
        last_name: survey.last_name,
      }
    })
    #Faraday.post("https://api.convertkit.com/v3/forms/1033788/subscribe", body, "Content-Type" => "application/json")
  end
end
