import { Controller } from "stimulus"

export default class extends Controller {
  static targets = [];

  connect() {
    if (!$(this.element).hasClass('dataTable')) {
      $(this.element).DataTable();
    };
  }
}
