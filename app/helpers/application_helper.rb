module ApplicationHelper
  def score_explanation(score)
    case score
    when 0..33
      "You are just starting out in your position and have a lot to learn"
    when 34..66
      "You are in the sweet spot and handle tasks well. However there is still more to learn."
    when 67..100
      "You have mastered your position and should think about leaping to a new curve."
    else
      "ERROR in your score"
    end
  end
end
