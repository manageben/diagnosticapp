## Setup
- Run `rails db:setup` to load the 10 questions into sqlite.

## Begin a Survey
- http://localhost:3000/
- Enter name & email (creates a new row in `surveys` table)

## Admin
- http://localhost:3000/admin
- u/pw in db/seeds.rb