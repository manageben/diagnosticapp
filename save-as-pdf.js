const puppeteer = require('puppeteer');

(async () => {
  // { args: ['--no-sandbox', '--disable-setuid-sandbox'] }
  const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] });
  const page = await browser.newPage();
  // await page.emulateMedia('print'); // default
  await page.goto(process.argv[2], {waitUntil: 'networkidle2'});
  await new Promise(resolve => setTimeout(resolve, 3200));
  await page.pdf({
    path: process.argv[3],
    format: 'letter',
    printBackground: true,
    pageRanges: '1'
  });

  await browser.close();
})();
