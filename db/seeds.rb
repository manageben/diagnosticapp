# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

puts "Creating Users"

users = [
  {
    name: 'Test',
    email: 'test@example.com',
    password: 'cactus stallion fidelity',
    password_confirmation: 'cactus stallion fidelity',
  }
]
users.each do |user|
  User.find_or_create_by(email: user[:email]) do |u|
    u.attributes = user
  end
end

puts "Creating Questions"

questions = [
  {
    category: "Performance in role",
    prompt: "When it comes to your performance in your current role, which statement below most accurately describes you?",
    low: "There is a lot of room for improvement. Either in the quality of what I am able to produce or in the time it takes me to achieve those outcomes.",
    mid: "Even though I still have opportunity and room to improve, my performance is on par with others who are in my role",
    high: "I'm considered the strongest performer in this role.",
    sort_order: 1,
  },
  {
    category: "Conceptualizing problems",
    prompt: "When it comes to your ability to conceptualize problems and approach them in your current role, which statement below most accurately describes you?",
    low: "When I encounter problems in my current role, it feels like I'm seeing this issue for the very first time and have to figure out what to do from scratch.",
    mid: "Most of the problems I encounter resemble something I've seen before and I have a starting point from which to approach a solution.",
    high: "It's a rare exception that I encounter a problem or issue that I haven't solved a number of times in this role previously.",
    sort_order: 2,
  },
  {
    category: "Learning and mastery",
    prompt: "When it comes to feeling like you learned what you need to learn to master your current role, which statement best describes you?",
    low: "Overall, I still have a lot to learn to master this role. I'm still trying to capture a vision for what my performance will look like once I've mastered what I'm doing.",
    mid: "I've learned enough to be proficient and competent. There is certainly more to learn, but I know where the gaps are and I know how to close them.",
    high: "I am considered one of the most knowledgeable and most skilled in this role.",
    sort_order: 3,
  },
  {
    category: "Subjective perception of challenge",
    prompt: "When it comes to how challenged you feel each day as you fulfill your responsibilities in your current role, which statement below best describes you?",
    low: "This current role is challenging, and honestly, some days it feels overwhelming and daunting. Like I'm never going to figure out what I'm doing.",
    mid: "This role is challenging, but I think I may have turned a corner - it's starting to feel manageable.",
    high: "I don't feel challenged in my current role. This job does not fully stretch my capabilities.",
    sort_order: 4,
  },
  {
    category: "Decisions and information",
    prompt: "When it comes to the amount of information and inputs flowing your way each day, which statement best describes how you feel?",
    low: "I'm early in my time at this role, so there is a constant stream of new information coming my way.",
    mid: "There is still new information coming my way, but I have a base of knowledge now that helps me feel like I am up to the task and can manage.",
    high: "New information doesn't come my way often, but when it does, I can easily manage because of my past experience.",
    sort_order: 5,
  },
  {
    category: "Training",
    prompt: "When it comes to the current level of training you feel you need to successfully complete your responsibilities in this role, which statement best describes how you feel?",
    low: "I'm definitely going to need more training before I feel like I've hit my stride in this role.",
    mid: "I feel competent in this role, but I could use some more training so that I can reach complete mastery.",
    high: "I feel completely qualified to provide training for anyone coming into this role, or for anyone who is looking to improve their performance in this role.",
    sort_order: 6,
  },
  {
    category: "Experience",
    prompt: "When others come to you to ask for advice, how do you feel about your current level of experience in this role?",
    low: "I'm enjoying the work, but I still feel a bit underwater and like I will need more experience to feel comfortable enough to give anyone advice.",
    mid: "Not an expert (yet), but I have enough experience that I can offer advice to those new to this role who need help.",
    high: "My experience in this role allows me to effectively advise others who are new.",
    sort_order: 7,
  },
  {
    category: "Help",
    prompt: "When it comes to getting help from others in your current role, which statement best describes how you feel?",
    low: "I'm still figuring out who has what level of expertise in our organization so I know who I can tap for help.",
    mid: "I have figured out who knows what throughout the organization, and I'm also finding that colleagues are starting to seek me out for help with more frequency.",
    high: "I used to be the person seeking advice and help, and now I'm one of the trusted voices that others come to for help.",
    sort_order: 8,
  },
  {
    category: "Growth",
    prompt: "When it comes to the amount of growth opportunity for you in this role, which statement below best describes how you feel?",
    low: "I'm seeing a lot of opportunity for me to grow and develop. It feels like there is a ton of room for growth.",
    mid: "I have already learned so much, but I still see that there are more opportunities for development in this same role.",
    high: "I feel like I've accomplished what I set out to do in this role and I can confidently move into a new role or path.",
    sort_order: 9,
  },
  {
    category: "How they feel",
    prompt: "When it comes to your overall happiness and satisfaction in your current role, which statement best describes how you feel?",
    low: "I'm really happy, but definitely still trying to figure things out.",
    mid: "I'm definitely enjoying this role. It's hard, but not too hard. Easy, but not too easy.",
    high: "I love this role, but I feel like I have things figured out.",
    sort_order: 10,
  },
]

questions.each do |question|
  Question.find_or_create_by(sort_order: question[:sort_order]) do |q|
    q.attributes = question
  end

end

puts "Creating Organisations"

# This organisation is used to map random team.
organisation = Organisation.find_or_create_by(name: "random organisation")

puts "Creating Teams"

# If user don't enter any name in optional field team
# we will map that survey to this team
Team.find_or_create_by(name: "random team") do |team|
  team.organisation_id = organisation.id
end