class ChangeColumnsOnQuestions < ActiveRecord::Migration[5.2]
  def change
    remove_column :questions, :question
    add_column :questions, :low, :string
    add_column :questions, :mid, :string
    add_column :questions, :high, :string
  end
end
