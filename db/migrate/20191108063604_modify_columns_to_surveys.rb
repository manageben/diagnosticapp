class ModifyColumnsToSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :surveys, :team_id, :integer
    rename_column :surveys, :team, :team_name
  end
end
