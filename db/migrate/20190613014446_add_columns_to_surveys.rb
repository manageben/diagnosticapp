class AddColumnsToSurveys < ActiveRecord::Migration[5.2]
  def change
    remove_column :surveys, :user_id
    add_column :surveys, :name, :string
    add_column :surveys, :email, :string
  end
end
