class AddEthnographyColumnsToSurveys < ActiveRecord::Migration[5.2]
  def change
    add_column :surveys, :age, :string
    add_column :surveys, :industry, :string
    add_column :surveys, :how_long_in_role, :string
    add_column :surveys, :gender, :string
    add_column :surveys, :intention_to_use_feedback, :string
    add_column :surveys, :next_steps, :text
    add_column :surveys, :what_information, :text
    add_column :surveys, :how_can_we_improve, :text
  end
end
