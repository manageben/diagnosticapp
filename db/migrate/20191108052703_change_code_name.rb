class ChangeCodeName < ActiveRecord::Migration[5.2]
  def change
    rename_column :surveys, :code, :team
  end
end
