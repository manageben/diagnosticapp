class AddIndexOnSortOrder < ActiveRecord::Migration[5.2]
  def change
    add_index :questions, :sort_order, unique: true
  end
end
