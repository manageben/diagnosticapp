class AddPromptToQuestions < ActiveRecord::Migration[5.2]
  def change
    add_column :questions, :prompt, :string
  end
end
